# SmartShares



Shareholder Management Solution for Banks

Restful API + Keycloak + Json web tokens + Oauth2 + docker

### Setting up

- Ensure you have the following installed on your PC:

    - docker
    - docker-compose


### Frameworks

- Spring Framework


### Assignment of Status codes

- `GET :: 200`
- `POST :: 202`
- `PUT :: 200`
- `DELETE :: 202`
